// Created by C.A. Gottardo on 29/06/2019.
// For the benefit of the FELIX users & developers
// Redesigned by Henk B, June 2020

#ifndef RASPBERRYIO_H
#define RASPBERRYIO_H

#include "TtcGenerator.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/mman.h>

// Access from RaspberryPi ARM running Linux
#define RPI3_PERIPH_BASE   0x3F000000
#define RPI4_PERIPH_BASE   0xFE000000
#define RPI3_GPIO_BASE     (RPI3_PERIPH_BASE + 0x200000) // GPIO controller
//#define RPI3_TIMERS_BASE (RPI3_PERIPH_BASE + 0x3000)
#define RPI4_GPIO_BASE     (RPI4_PERIPH_BASE + 0x200000) // GPIO controller

// GPIO setup macros.
// Always use INP_GPIO(x) (to clear bits) before using OUT_GPIO(x)
#define L1A_OUTPUT_PIN     22
#define ECR_OUTPUT_PIN     24
#define BUSY_INPUT_PIN     27
#define BLOCK_SIZE         (4*1024)

// Each GPIO has 3 bits for configuration,
// with bits for 10 (ten) GPIOs per 32-bit 'Function Select' register
// Function: input = 0, output = 1, other values = alternate functions
#define GPIO_TO_INPUT(g)  *(_gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define GPIO_TO_OUTPUT(g) {*(_gpio+((g)/10)) |=  (1<<(((g)%10)*3));\
                           *(_gpio+((g)/10)) &= ~(2<<(((g)%10)*3));\
                           *(_gpio+((g)/10)) &= ~(4<<(((g)%10)*3));}

// Register "GPIO PIN Output Set 0": set bits which are written as 1
#define GPIO_SET      *(_gpio+7)

// Register "GPIO PIN Output Clear 0": clears bits which are written as 1
#define GPIO_CLR      *(_gpio+10)

// Register "GPIO Pin Level 0": 0 if LOW, (1<<g) if HIGH
#define GET_GPIO(g)   (*(_gpio+13)&(1<<g))

class RaspberryIO: public TtcGenerator
{
public:
  RaspberryIO( std::ostream &log = std::cout );
  ~RaspberryIO();

  void l1a_set()      { GPIO_SET = 1 << L1A_OUTPUT_PIN; }
  void l1a_clear()    { GPIO_CLR = 1 << L1A_OUTPUT_PIN; }
  void ecr_set()      { GPIO_SET = 1 << ECR_OUTPUT_PIN; }
  void ecr_clear()    { GPIO_CLR = 1 << ECR_OUTPUT_PIN; }
  bool busy()         { return( GET_GPIO(BUSY_INPUT_PIN) == 0 ); }

  void setup_io();
  void setup_pins();

private:
  volatile uint32_t *_gpio;
  void *_gpioMap;
  bool  _isRpi4;
};

#endif // RASPBERRYIO_H
