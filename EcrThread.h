#ifndef ECRTHREAD_H
#define ECRTHREAD_H

#include <thread>
#include <iostream> // for std::cout

class TtcGenerator;
class L1aThread;

class EcrThread
{
 public:
  EcrThread( TtcGenerator *gen, L1aThread *l1a,
             std::ostream &log = std::cout );
  virtual ~EcrThread( );

 public:
  // General
  void start         ( );
  void stop          ( );
  void run           ( );
  void showSettings  ( );

  // ECR management
  bool isRunning     ( )                { return _running; }
  void setPeriod     ( unsigned int p ) { _ecrPeriodMs = p; }
  void setInhibit    ( unsigned int i ) { _ecrInhibitMs = i; }
  void setPulseWidth ( unsigned int w ) { _pulseWidthUs = w; }
  uint64_t ecrCount  ( )                { return _ecrCount; }

  // Timing
  void pause         ( uint64_t delay_ms );
  int  sleepOverhead ( );

 private:
  std::string timestamp( );

 private:
  // General
  std::thread  *_pThread;
  std::ostream &_log;

  // ECR management
  L1aThread   *_l1aThread;
  bool         _running;
  unsigned int _ecrPeriodMs;  // ECR period, in milliseconds
  unsigned int _ecrInhibitMs; // Trigger inhibit time before/after ECR, in ms
  unsigned int _pulseWidthUs;

  // Timing
  int _sleepOverhead; // Overhead in time of usleep()/sleep_for()

  // Statistics
  uint64_t _ecrCount;

  TtcGenerator *_ttcGen;
};
#endif // ECRTHREAD_H
