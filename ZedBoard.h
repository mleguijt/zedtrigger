#ifndef ZEDBOARD_H
#define ZEDBOARD_H

#include "TtcGenerator.h"

#include <iostream> // for std::cout
#include <string>
#include <map>
#include <vector>

// On the Nikhef picozed board Linux, in /sys/class/gpio:
// gpiochip1018, gpiochip1014 and gpiochip896
// (with 6, 4 and 118 GPIOs resp.)
// (matching /dev/gpiochip0, 1 and 2 resp.)
#define ZED_LED_0     1014
#define ZED_LED_1     1015
#define ZED_LED_2     1016
#define ZED_LED_3     1017

// For testing with LED visual feedback:
//#define ZED_L1A_OUT   ZED_LED_0
//#define ZED_ECR_OUT   ZED_LED_2
//#define ZED_BUSY_IN   ZED_LED_3
//#define ZED_GPIO_OFFS ZED_LED_0

// On device gpiochip108 | /dev/gpiochip0:
//#define ZED_L1A_OUT   1019
//#define ZED_ECR_OUT   1018
//#define ZED_BUSY_IN   1020
#define ZED_GPIO_OFFS 1018

// Using the register bank
// (see class RegsMem further down for the REG_xxx register indices)
#define ZED_L1A_OUT   REG_L1A
#define ZED_ECR_OUT   REG_ECR
#define ZED_BUSY_IN   REG_BUSY

// ----------------------------------------------------------------------------

class GPIO;
class GpioIoctl;
class GpioMem;
class RegsMem;

class ZedBoard: public TtcGenerator
{
public:
  ZedBoard( std::ostream &log = std::cout );
  virtual ~ZedBoard();

  void autotrigger( bool autotrigger, bool busy_enabled, int mode,
                    double freq_khz, uint32_t trig_count = 0 );

  void l1a_set   ( );
  void l1a_clear ( );
  void ecr_set   ( );
  void ecr_clear ( );
  bool busy      ( );
  int  l1a_gen   ( );
  void l1a_pause ( );
  void l1a_resume( );
  uint32_t busy_count( );

  void config_si5338( );

private:

  //GPIO *_gpio;
  //GpioIoctl *_gpio;
  //GpioMem *_gpio;
  RegsMem *_gpio;
  bool _autoTrigger;
};

// ----------------------------------------------------------------------------
/**
 * GPIO direction
 */
enum Direction
  {
   DIR_UNKNOWN, //!< Unknown/invalid
   DIR_IN,      //!< Input direction
   DIR_OUT      //!< Output direction
  };

// ----------------------------------------------------------------------------
/**
 * GPIO abstraction class (GPIO through /sys/class/gpio)
 */
class GPIO
{
public:
  GPIO( std::ostream &log = std::cout );
  virtual ~GPIO();

  void        enable       ( int pin, Direction dir );
  void        enable       ( int pin ) const;
  void        disable      ( int pin ) const;

  void        setDirection ( int pin, Direction dir );
  Direction   direction    ( int pin ) const;

  int         get          ( int pin ) const;
  void        set          ( int pin, int value ) const;

  int         getVal       ( int pin ) const;
  void        setVal       ( int pin, int value ) const;

private:
  void        write        ( const std::string &filename,
                             const std::string &content ) const;
  std::string read         ( const std::string &filename ) const;

  bool        pinNotOkay   ( int pin ) const;
  bool        pinExported  ( int pin ) const;
  std::string pinToFilename( int pin, const char *attribute ) const;

private:
  std::map<int, std::ifstream *> _inputs;
  std::map<int, std::ofstream *> _outputs;
  std::ostream &_log;
};

// ----------------------------------------------------------------------------
/**
 * GpioIoctl abstraction class (GPIO through ioctl() on /dev/gpiochipX)
 */
class GpioIoctl
{
public:
  GpioIoctl( std::ostream &log = std::cout, int chipnr = 1 );
  virtual ~GpioIoctl( );
  void enable ( int pin, Direction dir );
  void disable( int pin ) const { }
  int  getVal ( int pin ) const;
  void setVal ( int pin, int value ) const;
private:
  int                _fdGpioChip;
  std::map<int, int> _fdGpio;
  std::ostream      &_log;
};

// ----------------------------------------------------------------------------
/**
 * GpioMem abstraction class
 * (direct GPIO registers access through mmap() on /dev/mem)
 */
class GpioMem
{
public:
  GpioMem( std::ostream &log = std::cout );
  virtual ~GpioMem( );
  void enable ( int pin, Direction dir );
  void disable( int pin ) const { }
  int  getVal ( int pin ) const;
  void setVal ( int pin, int value ) const;
private:
  volatile uint32_t *_gpioRegs;
  std::ostream      &_log;
};

// ----------------------------------------------------------------------------
/**
 * RegsMem abstraction class
 * (register bank access through mmap() on /dev/mem)
 */

// Register indices and count
enum
  {
   REG_L1A = 0,
   REG_BCR,
   REG_ECR,
   REG_BCR_PERIOD,
   REG_OCR_PERIOD,
   REG_L1A_PERIOD,
   REG_TTC_CONFIG,
   REG_BUSY,
   REG_L1A_COUNT_READ,
   REG_OCR_COUNT_READ,
   REG_L1A_COUNT,
   REG_LUT_CONT_ENABLE,
   REG_L1A_AUTO_PAUSE,
   REG_UNUSED,
   REG_BUSY_ENABLE,
   REG_BUSY_PULSE_COUNT,
   REG_COUNT
  };

const std::vector<std::string> REG_NAME =
  {
   "L1A",
   "(BCR)",
   "ECR",
   "BCR_PERIOD",
   "(OCR_PERIOD)",
   "L1A_PERIOD",
   "TTC_CONFIG",
   "BUSY",
   "L1A_COUNT_READ",
   "(OCR_COUNT_READ)",
   "L1A_COUNT",
   "LUT_CONT_ENABLE",
   "L1A_AUTO_PAUSE",
   "<unused>",
   "BUSY_ENABLE",
   "BUSY_PULSE_COUNT"
  };

class RegsMem
{
public:
  RegsMem( std::ostream &log = std::cout );
  virtual ~RegsMem( );
  void enable ( int index, Direction dir ) { } // No operation
  void disable( int index ) const          { } // No operation
  int  getVal ( int index ) const;
  void setVal ( int index, int value ) const;
  void     setLut( int index, uint32_t value ) const;
  uint32_t getLut( int index ) const;
  void resetGth( );
private:
  volatile uint32_t *_regs;
  volatile uint32_t *_lut;
  volatile uint32_t *_lti;
  std::ostream      &_log;
};

// ----------------------------------------------------------------------------
#endif // ZEDBOARD_H
