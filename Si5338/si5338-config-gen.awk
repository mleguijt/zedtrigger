#! /usr/bin/awk -f
#
# For 'zedtrigger' Zynq development board;
# convert Si5338 ClockBuilder .h file to shell script of i2cset/get commands,
# with fixed I2C bus number 0 and I2C address 0x70:
# ./si5338-config-gen.awk Si5338-RevB-Registers >si5338-config.sh
#
# To generate the same as a string for inclusion in a C++ program
#  (e.g. to be executed line-by-line using calls to system()):
# ./si5338-config-gen.awk -v INCLUDEIT=1 Si5338-RevB-Registers >si5338-config.h
#

function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
function trim(s) { return rtrim(ltrim(s)); }

BEGIN {
    FS = ","
    setpage  = 1
    iset     = "i2cset -y"
    iget     = "i2cget -y"
    iaddr    = "0 0x70"
    isetaddr = iset " " iaddr
    igetaddr = iget " " iaddr
    if( INCLUDEIT == 1 )
        printf( "R\"=(\n" )
    printf( "#!/bin/sh -x\n" )
    printf( "#\n" )
    printf( "# *** NB! This is generated code, like this:\n" )
    printf( "# *** ./si5338-config-gen.awk %s >[this-file]\n", ARGV[ARGC-1] )
    printf( "#\n" )
    printf( "echo \"Preamble\"\n" )
    printf( "%s 255 0  # Set page bit to 0\n", isetaddr )
    printf( "%s -m 0x10 %s 230 0x10  # Set OEB_ALL = 1\n", iset, iaddr )
    printf( "%s -m 0x80 %s 241 0x80  # Set DIS_LOL = 1\n", iset, iaddr )
    printf( "\n" )
    printf( "echo \"Configuration...\"\n" )
}

substr( $1, 1, 1 ) == "{" {
    reg  = substr( $1, 2, length($1)-1 )
    reg  = ltrim( reg )
    val  = $2
    mask = substr( ltrim($3), 1, 4 )
    if( mask != "0x00" ) {
        if( mask == "0xFF" ) {
            printf( "%s %s %s\n", isetaddr, reg, val )
        } else {
            printf( "%s -m %s %s %s %s\n", iset, mask, iaddr, reg, val )
        }
        #printf( "%s * %s * %s\n", reg, val, mask )
    }
}

END {
    printf( "echo \"Done\"\n" )
    #printf( "%s 255 0  # Set page bit back to 0\n", isetaddr )
    printf( "\n" )
    printf( "echo \"Postamble\"\n" )
    printf( "echo \"Register 218:\"\n" )
    printf( "echo \"| 7 | 6 | 5 |    4    |    3     |    2      | 1 |    0    |\"\n" )
    printf( "echo \"|   |   |   | PLL_LOL | LOS_FDBK | LOS_CLKIN |   | SYS_CAL |\"\n" )
    printf( "stat=$(%s 218); echo \"stat(218) = $stat (-> Is input clock valid?)\"\n", igetaddr )
    printf( "%s -m 0x80 %s  49 0x00  # Set FCAL_OVRD_EN = 0\n", iset, iaddr )
    printf( "%s 246 0x02          # Set SOFT_RESET = 1\n", isetaddr )
    printf( "usleep 25000 # Wait 25 ms\n" )
    printf( "%s -m 0x80 %s 241 0x00  # Set DIS_LOL = 0\n", iset, iaddr )

    printf( "echo \"PLL lock status (-> is PLL locked?):\"\n" )
    printf( "stat=$(%s 218); echo \"stat(218) = $stat\" # bit4:PLL_LOL, bit0:SYS_CAL\n", igetaddr )
    printf( "usleep 100000 # Wait 100 ms\n" )
    printf( "stat=$(%s 218); echo \"stat(218) = $stat\" # bit4:PLL_LOL, bit0:SYS_CAL\n", igetaddr )
    printf( "usleep 100000 # Wait 100 ms\n" )
    printf( "stat=$(%s 218); echo \"stat(218) = $stat\" # bit4:PLL_LOL, bit0:SYS_CAL\n", igetaddr )

    printf( "\n" )
    printf( "echo \"Copy FCAL values\"\n" )
    printf( "fcal=$(%s 237); %s -m 0x03 %s 47 $fcal\n", igetaddr, iset, iaddr )
    printf( "fcal=$(%s 236); %s -m 0xFF %s 46 $fcal\n", igetaddr, iset, iaddr )
    printf( "fcal=$(%s 235); %s -m 0xFF %s 45 $fcal\n", igetaddr, iset, iaddr )
    printf( "%s -m 0xFC %s  47 0x14\n", iset, iaddr )

    printf( "\n" )
    printf( "%s -m 0x80 %s  49 0x80  # Set FCAL_OVRD_EN = 1\n", iset, iaddr )
    printf( "%s -m 0x10 %s 230 0x00  # Set OEB_ALL = 0\n", iset, iaddr )

    if( INCLUDEIT == 0 ) {
        printf( "\n" )
        printf( "echo \"Reset GTH\"\n" )
        printf( "devmem 0x80020000 32 1\n" )
        printf( "devmem 0x80020000 32 0\n" )
    }

    if( INCLUDEIT == 1 )
        printf( ")=\";\n" )
}
