#include "RaspberryIO.h"

// ----------------------------------------------------------------------------

RaspberryIO::RaspberryIO( std::ostream &log )
  : TtcGenerator( log ),
    _gpio( 0 ),
    _gpioMap( 0 ),
    _isRpi4( false )
{
  // Find out what type of RPi: model 3 or model 4
  //int fd = open( "/proc/cpuinfo", O_RDONLY );
  int fd = open( "/proc/device-tree/model", O_RDONLY );
  if( fd >= 0 )
   {
     //std::string str( 2048, 0 );
     std::string str( 256, 0 );
     ssize_t s = read( fd, (void *) str.data(), str.size() );
     //if( str.find( "BCM27" ) != std::string::npos )
     if( str.find( "Pi 4" ) != std::string::npos )
       _isRpi4 = true;
     if( str.length() > 0 )
       printf( "%s\n", str.c_str() );
   }

  // Set up and initialize
  setup_io();
  setup_pins();
  l1a_clear();
}

// ----------------------------------------------------------------------------

RaspberryIO::~RaspberryIO()
{
}

// ----------------------------------------------------------------------------

// Set up a memory region to access GPIO
void RaspberryIO::setup_io()
{
  // Open /dev/mem
  int mem_fd;
  if( (mem_fd = open( "/dev/mem", O_RDWR|O_SYNC )) < 0 ) {
    printf( "can't open /dev/mem \n" );
    exit( -1 );
  }

  // mmap GPIO
  off_t gpio_base;
  if( _isRpi4 )
    gpio_base = RPI4_GPIO_BASE;
  else
    gpio_base = RPI3_GPIO_BASE;
  _gpioMap = mmap(
      NULL,             // Any adddress in our space will do
      BLOCK_SIZE,       // Map length
      PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
      MAP_SHARED,       // Shared with other processes
      mem_fd,           // File to map
      gpio_base         // Offset to GPIO peripheral
  );

  close(mem_fd); // No need to keep mem_fd open after mmap

  if( _gpioMap == MAP_FAILED ) {
    printf( "mmap error %d\n", (int)(size_t)_gpioMap ); // errno also set!
    exit( -1 );
  }

  // Always use volatile pointer!
  _gpio = (volatile uint32_t *) _gpioMap;
}

// ----------------------------------------------------------------------------

void RaspberryIO::setup_pins()
{
  // Set GPIO outputs and inputs
  // (Note: GPIO_TO_INPUT() for output pins may cause extra pulse (pull-up?),
  //        so GPIO_TO_OUTPUT() was modified, and the GPIO_TO_INPUT()
  //        call on output pins was removed; Henk B, March 2022)

  GPIO_TO_OUTPUT( L1A_OUTPUT_PIN );

  GPIO_TO_OUTPUT( ECR_OUTPUT_PIN );

  GPIO_TO_INPUT( BUSY_INPUT_PIN );
  // Enable pull-up ?
  // ...
}

// ----------------------------------------------------------------------------
