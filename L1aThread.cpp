#include <ctime>
#include <random>
#include <iomanip>
#include <cmath> // for log10()
#include "L1aThread.h"

// ----------------------------------------------------------------------------

L1aThread::L1aThread( TtcGenerator *gen, std::ostream &log )
  : _pThread( 0 ),
    _log( log ),
    _running( false ),
    _autoTrigger( false ),
    _doPause( false ),
    _paused( false ),
    _triggerFreq( 1.0 ),
    _triggerMode( L1A_RATE_CONST ),
    _triggerCount( 0 ),
    _pulseWidth( 100 ),
    _busyEnabled( true ),
    _sleepOverhead( 0 ),
    _gettimeOverhead( 0 ),
    _triggers( 0 ),
    _triggersSuppressed( 0 ),
    _ttcGen( gen )
{
  // Determine the usleep() call overhead
  _sleepOverhead = this->sleepOverhead();

  // Determine the clock_gettime() call overhead
  _gettimeOverhead = this->gettimeOverhead();
}

// ----------------------------------------------------------------------------

L1aThread::~L1aThread()
{
  // In case the thread is still running
  this->stop();
}

// ----------------------------------------------------------------------------

void L1aThread::start()
{
  //_log << "(DEBUG: L1aThread::start)" << std::endl;
  if( _pThread == 0 )
    _pThread = new std::thread( [this](){ this->run(); } );
}

// ----------------------------------------------------------------------------

void L1aThread::stop()
{
  //_log << "(DEBUG L1aThread::stop)" << std::endl;
  _running = false;

  if( _pThread )
    {
      // Wait until the receive thread (i.e. function run()) exits
      _pThread->join();
      _pThread = 0;
    }
}

// ----------------------------------------------------------------------------

void L1aThread::pause()
{
  _doPause = true;
  if( _running )
    while( !_paused ) std::this_thread::yield();
  if( _autoTrigger )
    _ttcGen->l1a_pause();
}

// ----------------------------------------------------------------------------

void L1aThread::resume()
{
  _doPause = false;
  if( _autoTrigger )
    _ttcGen->l1a_resume();
}

// ----------------------------------------------------------------------------

void L1aThread::run()
{
  //_log << "(L1aThread started)" << std::endl;

  _triggers = 0;
  _triggersSuppressed = 0;

  // Fill array of delays (to allow for certain time distributions),
  // in nanoseconds (_triggerFreq is in KHz)
  uint64_t d;
  if( _triggerFreq == 0 )
    {
      // Go for maximum rate
      for( int i=0; i<L1A_DELAY_SZ; ++i )
        _delay[i] = 0;
    }
  else if( _triggerMode == L1A_RATE_GAUSS )
    {
      // Gaussian distribution
      // (68% of the time the rate variation is within 10%)
      double hi = _triggerFreq + 0.1*_triggerFreq;
      double lo = _triggerFreq - 0.1*_triggerFreq;
      double sigma = (hi - lo)/2.0;
      std::normal_distribution<double> distr( _triggerFreq, sigma );
      std::random_device randev;
      for( int i=0; i<L1A_DELAY_SZ; ++i )
        {
          // Delay in ns
          d = (uint64_t) (1000000000.0/(distr(randev)*1000.0));
          _delay[i] = d;

          // DEBUG
          //if( i < 100 )
          //  _log << d << std::endl;
        }
    }
  else if( _triggerMode == L1A_RATE_RANDOM )
    {
      // Random distribution (exponential)
      std::uniform_real_distribution<double> distr( 0.0000001, 1.0 );
      std::random_device randev;
      uint64_t d_avg = (uint64_t)(1000000000.0/(_triggerFreq*1000.0));
      for( int i=0; i<L1A_DELAY_SZ; ++i )
        {
          // Delay in ns
          d = (uint64_t) (-log(distr(randev)) * (double) d_avg);
          if( d == 0 ) d = 1;
          _delay[i] = d;

          // DEBUG
          //if( i < 100 )
          //  _log << d << std::endl;
        }
    }
  else
    {
      // Constant
      d = (uint64_t) (1000000000.0/(_triggerFreq*1000.0));
      for( int i=0; i<L1A_DELAY_SZ; ++i )
        {
          // Delay in ns
          _delay[i] = d;
        }
    }

  // Apply timing corrections
  /* NB: No need to correct when using the 'delay_start' parameter in pause()
         so this has been removed, in particular _otherOverhead
  for( int i=0; i<L1A_DELAY_SZ; ++i )
    {
      d = _delay[i];

      // Subtract pulse width delay
      if( d >= _pulseWidth )
        d -= _pulseWidth;
      else
        d = 0;

      // Subtract 'other' overhead (= configurable 'calibration constant')
      if( d >= _otherOverhead )
        d -= _otherOverhead;
      else
        d = 0;

      _delay[i] = d;
    }
  */
  // DEBUG
  //_log << "d=" << _delay[0] << std::endl;
  //_log << "f=" << _triggerFreq << ", delay="
  //     << _delay[0] << " " << _delay[L1A_DELAY_SZ-1] << std::endl;

  struct timespec ts;
  uint64_t delay_start;
  clock_gettime( CLOCK_REALTIME, &ts );
  delay_start = (uint64_t) ts.tv_sec*1000000000 + (uint64_t) ts.tv_nsec;

  bool busy;
  int delay_index=0;
  _running = true;
  while( _running )
    {
      _paused = _doPause;

      if( _triggerCount > 0 &&
          _triggers >= _triggerCount )
        _paused = true; // Number of triggers reached, pause indefinitely

      if( !_paused )
        {
          if( _ttcGen )
            busy = _ttcGen->busy();
          else
            busy = false;

          if( !(busy && _busyEnabled) )
            _ttcGen->l1a_set();

          pause( _pulseWidth );
          //pause( 50*1000*1000 ); // 50ms TEST

          if( !(busy && _busyEnabled) )
            _ttcGen->l1a_clear();

          if( busy && _busyEnabled )
            ++_triggersSuppressed;
          else
            ++_triggers;
        }

      _paused = _doPause;

      if( !_paused )
        {
          pause( _delay[delay_index], &delay_start );
        }
      else
        {
          // In paused state: sleep a minimum amount of time
          // to be able to resume quickly (in case of a low L1A rate)
          if( _delay[delay_index] < 2*_sleepOverhead )
            pause( _delay[delay_index] );
          else
            pause( 2*_sleepOverhead );
        }

      // Size of the delay array is a power of 2
      delay_index = (delay_index + 1) & (L1A_DELAY_SZ-1);
    }
}

// ----------------------------------------------------------------------------

bool L1aThread::monitor( int seconds )
{
  struct timespec tspec1, tspec2;
  uint64_t start_ns, duration_ns;
  uint64_t t0, t1;
  uint64_t ts0, ts1;
  int secs = 0;
  uint32_t busy_begin, busy_end;

  // Sync with start-up of run()
  while( !_running )
    std::this_thread::sleep_for( std::chrono::microseconds(1) );

  int cntr_width = 1;
  if( seconds > 9 )
    cntr_width = log10(seconds) + 1;

  _log << std::fixed << std::setprecision(3);
  int busy_cnt = 0;
  if( _autoTrigger )
    {
      t0  = _ttcGen->l1a_gen();
      ts0 = 0;
      busy_begin = _ttcGen->busy_count();
    }
  else
    {
      t0  = _triggers;
      ts0 = _triggersSuppressed;
    }
  clock_gettime( CLOCK_REALTIME, &tspec1 );
  start_ns = ((uint64_t) tspec1.tv_sec*1000000000 +
              (uint64_t) tspec1.tv_nsec);
  while( true )
    {
      std::this_thread::sleep_for( std::chrono::seconds(1) );

      if( _autoTrigger )
        {
          t1  = _ttcGen->l1a_gen();
          ts1 = 0;
          busy_end = _ttcGen->busy_count();
        }
      else
        {
          t1  = _triggers;
          ts1 = _triggersSuppressed;
        }
      clock_gettime( CLOCK_REALTIME, &tspec2 );
      duration_ns = ((uint64_t) tspec2.tv_sec*1000000000 +
                     (uint64_t) tspec2.tv_nsec) - start_ns;

      // Rather than assuming 'exactly' 1 second (sleep(1)),
      // derive the duration from the system clock
      //_log << t1 << " " << t0 << std::endl;
      double rate = (((double) (t1 - t0)) /
                     (((double) duration_ns)/1000000.0))*1000.0; // Hz
      double busy_percentage;

      if( _autoTrigger )
        {
          uint64_t busy_ns;
          // Wrap-around? (32 bits)
          if( busy_begin > busy_end )
            busy_ns = (uint64_t) 0x100000000 - busy_begin + busy_end;
          else
            busy_ns = (uint64_t) busy_end - (uint64_t) busy_begin;
          busy_ns *= (uint64_t) 25; // About 25 ns per count (40.079MHz)

          busy_percentage = (100.0 * (double) busy_ns) / (double) duration_ns;
          if( busy_percentage > (double) 100.0 )
            busy_percentage = (double) 100.0;
        }
      else
        {
          if( t1-t0 + ts1-ts0 != 0 )
            busy_percentage = (100.0 * ((double) (ts1-ts0)) /
                               (double)(t1-t0 + ts1-ts0));
          else
            busy_percentage = 0.0;
        }

      if( busy_percentage > 0.0 )
        ++busy_cnt;

      if( seconds > 0 )
        _log << std::setw(cntr_width) << secs+1 << " ";
      _log << timestamp() << " " << std::setw(7);
      if( rate > 900.0 )
        _log << rate/1000.0 << " kHz";
      else
        _log << rate << " Hz";
      _log << "  busy: ";
      if( busy_percentage == 0.0 )
        _log << std::setprecision(0);
      _log << busy_percentage << "\%" << std::endl << std::setprecision(3);

      ++secs;
      //if( secs == seconds || (_triggerCount > 0 && _triggers==_triggerCount) )
      if( secs == seconds || (_triggerCount > 0 && t1 >= _triggerCount) )
        break;

      t0  = t1;
      ts0 = ts1;
      start_ns = start_ns + duration_ns;
      busy_begin = busy_end;
    }

  // If more than half the above monitor-loops resulted in a busy fraction
  // larger than zero, this function returns false, otherwise returns true
  // (this procedure may be adjusted in a future version)
  return( busy_cnt < secs/2 );
}

// ----------------------------------------------------------------------------

void L1aThread::showSettings()
{
  _log << "Auto-trigger      = " << (_autoTrigger ? "YES" : "NO") << std::endl;
  _log << "Requested freq    = " << _triggerFreq << " (";
  if( _triggerFreq == 0 )
    _log << "max rate";
  else if( _triggerMode == L1A_RATE_CONST )
    _log << "const";
  else if( _triggerMode == L1A_RATE_RANDOM )
    _log << "random";
  else if( _triggerMode == L1A_RATE_GAUSS )
    _log << "gaussian";
  else
    _log << '?' << '?' << '?';
  _log << ")" << std::endl;
  if( _triggerCount > 0 )
    _log << "triggerCount      = " << _triggerCount << std::endl;
  if( !_autoTrigger )
    _log << "pulseWidth        = " << _pulseWidth    << " ns" << std::endl
         << "sleepOverhead     = " << _sleepOverhead << " ns" << std::endl
         << "gettimeOverhead   = " << _gettimeOverhead << " ns" << std::endl;
  _log << "busyEnabled       = " << (_busyEnabled ? "YES" : "NO") << std::endl;
}

// ----------------------------------------------------------------------------
// Private functions
// ----------------------------------------------------------------------------

inline void L1aThread::pause( uint64_t delay_ns, uint64_t *delay_start )
{
  // Use usleep() when appropriate, but run a busy-wait loop for short delays
  // (as determined by usleep() overhead) to achieve better timing precision
  // for shorter delays
  if( delay_ns > 2*_sleepOverhead )
    {
      //usleep( ((delay_ns - _sleepOverhead)+500)/1000 );
      std::this_thread::sleep_for( std::chrono::nanoseconds( delay_ns -
                                                             _sleepOverhead) );
    }
  else if( delay_ns > 0 )
    {
      struct timespec ts;
      volatile int64_t s, t;
      if( delay_start )
        {
          s = *delay_start + delay_ns;
        }
      else
        {
          clock_gettime( CLOCK_REALTIME, &ts );
          s = (int64_t) ts.tv_sec*1000000000 + (int64_t) ts.tv_nsec;
          s += delay_ns;
        }
      clock_gettime( CLOCK_REALTIME, &ts );
      t = (int64_t) ts.tv_sec*1000000000 + (int64_t) ts.tv_nsec;
      while( t < s )
        {
          clock_gettime( CLOCK_REALTIME, &ts );
          t = (int64_t) ts.tv_sec*1000000000 + (int64_t) ts.tv_nsec;
        }
      if( delay_start )
        *delay_start = s;
    }
}

// ----------------------------------------------------------------------------

int L1aThread::sleepOverhead()
{
  // Determine the time overhead it takes to execute usleep(1) or sleep_for()
  struct timespec ts1, ts2;
  clock_gettime( CLOCK_REALTIME, &ts1 );
  // Average over 4 calls..
  //usleep( 1 ); usleep( 1 ); usleep( 1 ); usleep( 1 );
  std::this_thread::sleep_for( std::chrono::nanoseconds(1000000) );
  std::this_thread::sleep_for( std::chrono::nanoseconds(1000000) );
  std::this_thread::sleep_for( std::chrono::nanoseconds(1000000) );
  std::this_thread::sleep_for( std::chrono::nanoseconds(1000000) );
  clock_gettime( CLOCK_REALTIME, &ts2 );
  // t in nanoseconds
  int64_t t = (((int64_t) ts2.tv_sec*1000000000 + (int64_t) ts2.tv_nsec) -
               ((int64_t) ts1.tv_sec*1000000000 + (int64_t) ts1.tv_nsec))/4;

  // Subtract the 1000 ns sleep time and return as micro- or nanoseconds
  //return (t-1000000+500)/1000; // Return microseconds
  return (t-1000000);            // Return nanoseconds
}

// ----------------------------------------------------------------------------

int L1aThread::gettimeOverhead()
{
  // Determine the time overhead it takes to execute usleep(1) or sleep_for()
  struct timespec ts1, ts2, dummy1, dummy2;
  clock_gettime( CLOCK_REALTIME, &ts1 );
  // Average over 10 calls..
  clock_gettime( CLOCK_REALTIME, &dummy1 );
  clock_gettime( CLOCK_REALTIME, &dummy2 );
  clock_gettime( CLOCK_REALTIME, &dummy1 );
  clock_gettime( CLOCK_REALTIME, &dummy2 );
  clock_gettime( CLOCK_REALTIME, &dummy1 );
  clock_gettime( CLOCK_REALTIME, &dummy2 );
  clock_gettime( CLOCK_REALTIME, &dummy1 );
  clock_gettime( CLOCK_REALTIME, &dummy2 );
  clock_gettime( CLOCK_REALTIME, &dummy1 );
  clock_gettime( CLOCK_REALTIME, &ts2 );
  // t in nanoseconds
  int64_t t = (((int64_t) ts2.tv_sec*1000000000 + (int64_t) ts2.tv_nsec) -
               ((int64_t) ts1.tv_sec*1000000000 + (int64_t) ts1.tv_nsec))/10;

  return (int) t; // Return nanoseconds
}

// ----------------------------------------------------------------------------

std::string L1aThread::timestamp()
{
  time_t now;
  char the_date[24];
  now = time(NULL);
  strftime(the_date, 24, "[%H:%M:%S %d.%m.%Y]", localtime(&now));
  return std::string(the_date);
}

// ----------------------------------------------------------------------------
