#include <stdio.h>
#include <signal.h>

#include "TtcviModule.h"

static int MapId;

// ----------------------------------------------------------------------------
// Class TtcviModule
// ----------------------------------------------------------------------------

TtcviModule::TtcviModule( std::ostream &log )
  : TtcGenerator( log ),
  _ttcVi( 0 )
{
  unsigned long ttcvi_virtual_addr;
  if( open_ttcvi( &ttcvi_virtual_addr ) == VME_SUCCESS )
    {
      _ttcVi = (TTCVI *) ttcvi_virtual_addr;
      // Going to generate triggers using a VME write cycle
      _ttcVi->ttc_Csr1 = TTC_CSR1_TRIGSELECT_VME;
      // Generate a BCR (=1, ECR=2, BCR+ECR=3)
      _ttcVi->ttc_ShortVmeCycl = 0x01;
    }
}

// ----------------------------------------------------------------------------

TtcviModule::~TtcviModule()
{
  if( _ttcVi )
    close_ttcvi();
  _ttcVi = 0;
}

// ----------------------------------------------------------------------------
// Global functions
// ----------------------------------------------------------------------------

void buserr_handler( int arg )
{
  VME_ErrorCode_t    error_code;
  VME_BusErrorInfo_t businfo;

  error_code = VME_BusErrorInfoGet (&businfo);
  printf ("-> BusError <-\n");
}

// ----------------------------------------------------------------------------

int open_ttcvi( unsigned long *pttcvi_virtual_addr )
{
  VME_ErrorCode_t error_code;
  VME_MasterMap_t master_map;

  signal( SIGBUS, buserr_handler );

  error_code = VME_Open();
  if( error_code != VME_SUCCESS )
    {
      printf( "Error VME_Open\n" );
      VME_ErrorPrint( error_code );
      return( error_code );
    }

  {
    struct sigaction sa;
    sa.sa_handler = buserr_handler;
    sa.sa_flags   = 0;
    sigemptyset( &sa.sa_mask );
    sigaction( SIGUSR1, &sa, NULL );
  }

  // This doesn't work properly!
  error_code = VME_BusErrorRegisterSignal( SIGUSR1 );
  if( error_code != VME_SUCCESS )
    {
      printf( "Error VME_BusErrorRegisterSignal\n" );
      VME_ErrorPrint( error_code );
    }

  master_map.vmebus_address   = TTCVI_OFFS;
  master_map.window_size      = TTCVI_SIZE;
  master_map.address_modifier = VME_AM39;
  master_map.options          = 0x00;

  error_code = VME_MasterMap( &master_map, &MapId );
  if( error_code != VME_SUCCESS )
    {
      printf( "Error VME_MasterMap TTCvi\n" );
      VME_ErrorPrint( error_code );
      return( error_code );
    }

  error_code = VME_MasterMapVirtualLongAddress( MapId, pttcvi_virtual_addr );
  if( error_code != VME_SUCCESS )
    {
      printf( "Error VME_MasterMapVirtualAddress TTCvi\n" );
      VME_ErrorPrint( error_code );
      return( error_code );
    }

  return( error_code );
}

// ----------------------------------------------------------------------------

int close_ttcvi( void )
{
  VME_ErrorCode_t error_code;

  error_code = VME_MasterUnmap( MapId );
  if( error_code != VME_SUCCESS )
    {
      printf( "Error VME_MasterUnMap\n" );
      VME_ErrorPrint( error_code );
      return( error_code );
    }

  VME_BusErrorRegisterSignal( 0 );
  VME_Close();

  return( error_code );
}

// ----------------------------------------------------------------------------
